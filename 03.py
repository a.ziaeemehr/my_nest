#!/usr/lib/env python

import nest
import pylab as pl
neuron1 = nest.Create("iaf_psc_alpha")
nest.SetStatus(neuron1, {"I_e": 376.0})
neuron2 = nest.Create("iaf_psc_alpha")
multimeter = nest.Create("multimeter")
nest.SetStatus(multimeter, {"withtime": True,
                            "record_from": ["V_m"]})
# connect neuron1 to neuron2
nest.Connect(neuron1, neuron2, syn_spec={"weight": 20.0})
# record the membrane potential from neuron2 
nest.Connect(multimeter, neuron2)
#  default delay of 1ms, alternative command is:
nest.Connect(neuron1,
             neuron2, syn_spec={"weight": 20, "delay": 1.0})
nest.Simulate(1000.0)  # in ms
dmm = nest.GetStatus(multimeter)[0]
Vms = dmm["events"]["V_m"]
ts = dmm["events"]["times"]
pl.plot(ts, Vms)
pl.show()
