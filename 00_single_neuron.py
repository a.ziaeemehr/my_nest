#!/usr/bin/env python

import nest
import pylab as pl
import pylab
from sys import exit

neuron = nest.Create('iaf_psc_alpha')
nest.GetStatus(neuron, "I_e")
nest.GetStatus(neuron, ["V_reset", "V_th"])
nest.SetStatus(neuron,{'I_e':376.0})
multimeter = nest.Create("multimeter")
nest.SetStatus(multimeter,{"withtime":True,
                           "record_from":["V_m"]})
spikedetector = nest.Create("spike_detector",
                            params={"withgid": True,
                                   "withtime": True})
# withgid indicates whether the spike detector is to 
# record the source id from which it received the event
# (i.e. the id of our neuron).                                   

# Connecting nodes with default connections
nest.Connect(multimeter, neuron)
nest.Connect(neuron, spikedetector)
nest.Simulate(1000.0) # in ms
# Extracting and plotting data from devices
dmm = nest.GetStatus(multimeter)[0]
Vms = dmm["events"]["V_m"]
ts = dmm["events"]["times"]
pl.figure(1)
pl.plot(ts,Vms)

dSD = nest.GetStatus(spikedetector,keys='events')[0]
evs = dSD['senders']
ts = dSD["times"]
pylab.figure(2)
pylab.plot(ts,evs,'.')
pl.show()


