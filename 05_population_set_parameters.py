#!/usr/lib/env python

import nest
import pylab as pl
from sys import exit
import numpy as np

neuron1 = nest.Create("iaf_psc_alpha")
ndict = {"I_e": 200.0, "tau_m": 20.0}
nest.SetDefaults("iaf_psc_alpha",ndict)
neuronpop1 = nest.Create("iaf_psc_alpha", 100)
neuronpop2 = nest.Create("iaf_psc_alpha", 100)
neuronpop3 = nest.Create("iaf_psc_alpha", 100)
# -------------------------------------------------- #
edict = {"I_e": 200.0, "tau_m": 20.0}
nest.CopyModel("iaf_psc_alpha", "exc_iaf_neuron")
nest.SetDefaults("exc_iaf_neuron",edict)

# or in one step ----------------------------------- #
idict = {"I_e": 300.0}
nest.CopyModel("iaf_psc_alpha", "inh_iaf_neuron", params=idict)
epop1 = nest.Create("exc_iaf_neuron", 100)
epop2 = nest.Create("exc_iaf_neuron", 100)
ipop1 = nest.Create("inh_iaf_neuron", 30)
ipop2 = nest.Create("inh_iaf_neuron", 30)

# populations with an inhomogeneous set of parameters
parameter_list = [{"I_e": 200.0, "tau_m": 20.0},
                  {"I_e": 150.0, "tau_m": 30.0}]
epop3 = nest.Create("exc_iaf_neuron", 2, parameter_list)            

Vth = -55.0
Vrest = -70.0
for neuron in epop1:
    nest.SetStatus([neuron],
    {"V_m": Vrest+(Vth-Vrest)*np.random.rand()})
# print epop1

dVms =  [{"V_m": Vrest+(Vth-Vrest)\
                 *np.random.rand()} for x in epop1]
nest.SetStatus(epop1, dVms)
# print dVms
# or more concise if we want to randomize only one parameter
Vms = Vrest+(Vth-Vrest)*np.random.rand(len(epop1))
nest.SetStatus(epop1, "V_m",Vms)
