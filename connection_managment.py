#!/usr/lib/env python
'''
http://www.nest-simulator.org/connection-management/
'''

import nest
import pylab as pl
from sys import exit
import numpy as np

# Connect(pre, post)
# Connect(pre, post, conn_spec)
# Connect(pre, post, conn_spec, syn_spec)

## Connection Rules
# one to one
n = 10
A = nest.Create("iaf_psc_alpha", n)
B = nest.Create("spike_detector", n)
nest.Connect(A, B, "one_to_one")

nest.Connect(A, B, "one_to_one", 
             syn_spec={"weight": 1.5, "delay": 0.5})
# only two node are connected with this properties
weight = 1.5
delay = 0.5
# nest.Connect(A[0], B[0], weight, delay)  # Error

# all to all
n ,m = 10, 12
A = nest.Create("iaf_psc_alpha", n)
B = nest.Create("iaf_psc_alpha", m)
nest.Connect(A, B)

# fixed-indegree
n, m, N = 12, 12, 2
A = nest.Create("iaf_psc_alpha", n)
B = nest.Create("iaf_psc_alpha", m)
conn_dict = {"rule": "fixed_indegree", "indegree": N}
nest.Connect(A, B, conn_dict)

# fixed-outdegree
n, m, N = 12, 12, 2
A = nest.Create("iaf_psc_alpha", n)
B = nest.Create("iaf_psc_alpha", m)
conn_dict = {"rule": "fixed_outdegree", "outdegree": N}
nest.Connect(A, B, conn_dict)

# fixed-total-number
n, m, N = 12, 12, 30
A = nest.Create("iaf_psc_alpha", n)
B = nest.Create("iaf_psc_alpha", m)
conn_dict = {"rule": "fixed_total_number", "N": N}
nest.Connect(A, B, conn_dict)

# pairwise-bernoulli
# For each possible pair of nodes from pre and post,
# a connection is created with probability p.
n, m, N = 12, 12, 30
A = nest.Create("iaf_psc_alpha", n)
B = nest.Create("iaf_psc_alpha", m)
conn_dict = {"rule": "pairwise_bernoulli", "N": N}
nest.Connect(A, B, conn_dict)

## Synapse Specification
n = 10
A = nest.Create("iaf_psc_alpha", n)
B = nest.Create("iaf_psc_alpha", n)
nest.CopyModel("static_synapse","excitatory",
                {"weight": 2.5, "delay":0.5})
nest.Connect(A, B, syn_spec="excitatory")
