#!/usr/bin/env python

import nest
import pylab as pl
import pylab
from sys import exit

neuron = nest.Create('iaf_psc_alpha')
nest.GetStatus(neuron, "I_e")
nest.GetStatus(neuron, ["V_reset", "V_th"])
nest.SetStatus(neuron,{'I_e':376.0})
multimeter = nest.Create("multimeter")
nest.SetStatus(multimeter,{"withtime":True,
                           "record_from":["V_m"]})
spikedetector = nest.Create("spike_detector",
                            params={"withgid": True,
                                   "withtime": True})
# withgid indicates whether the spike detector is to 
# record the source id from which it received the event
# (i.e. the id of our neuron).                                   

# Connecting nodes with default connections
nest.Connect(multimeter, neuron)
nest.Connect(neuron, spikedetector)
nest.Simulate(1000.0) # in ms

# Create an extra neuron
neuron2 = nest.Create("iaf_psc_alpha")
nest.SetStatus(neuron2,{"I_e":370.0})
# connect this newly created neuron to the multimeter
nest.Connect(multimeter, neuron2)
nest.Connect(neuron2, spikedetector)
# plot the results
dmm = nest.GetStatus(multimeter)[0]
Vms1 = dmm["events"]["V_m"][::2]
ts1 = dmm["events"]["times"][::2]
pl.plot(ts1,Vms1)
Vms2 = dmm["events"]["V_m"][1::2]
ts2 = dmm['events']['times'][1::2]
pl.plot(ts2,Vms2)

# ------------------------------------------------------- #
# Connecting nodes with specific connections
# neuron receives 2 Poisson spike trains,
# one excitatory and the other inhibitory

noise_ex = nest.Create("poisson_generator")
noise_in = nest.Create("poisson_generator")
noise = [noise_ex, noise_in]
nest.SetStatus(noise_ex, {"rate": 80000.0})
nest.SetStatus(noise_in, {"rate": 15000.0})
# constant input current should be set to 0:
nest.SetStatus(neuron, {"I_e": 0.0})

noise = [noise_ex,noise_in]
# excitatory postsynaptic current of 1.2pA amplitude
syn_dict_ex = {"weight": 1.2} 
# inhibitory postsynaptic current of -2pA amplitude
syn_dict_in = {"weight": -2.0}
nest.Connect([noise[0]], neuron, syn_spec=syn_dict_ex)
nest.Connect([noise[1]], neuron, syn_spec=syn_dict_in)


# #### Two connected neurons

# In[58]:

import pylab
import nest
neuron1 = nest.Create("iaf_psc_alpha")
nest.SetStatus(neuron1,{"I_e": 376.0})
neuron2 = nest.Create("iaf_psc_alpha")
multimeter = nest.Create("multimeter")
nest.SetStatus(multimeter, {"withtime":True, 
                            "record_from":["V_m"]})
nest.Simulate(1000.0)


# In[59]:

nest.Connect(neuron1, neuron2, syn_spec = {"weight":20.0})
nest.Connect(multimeter, neuron2)


# In[60]:

nest.Connect(neuron1, neuron2, syn_spec={"weight":20, "delay":1.0})


# In[57]:

dmm2 = nest.GetStatus(multimeter)[0]
Vms1 = dmm2["events"]["V_m"][::2]
ts1 = dmm2["events"]["times"][::2]
pylab.plot(ts1,Vms1)


# In[ ]:





