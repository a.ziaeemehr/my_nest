#!/usr/bin/env python

import nest
import pylab as pl
import pylab
from sys import exit

neuron = nest.Create('iaf_psc_alpha')
nest.GetStatus(neuron, "I_e")
nest.GetStatus(neuron, ["V_reset", "V_th"])
nest.SetStatus(neuron,{'I_e':376.0})
multimeter = nest.Create("multimeter")
nest.SetStatus(multimeter,{"withtime":True,
                           "record_from":["V_m"]})
spikedetector = nest.Create("spike_detector",
                            params={"withgid": True,
                                   "withtime": True})
# withgid indicates whether the spike detector is to 
# record the source id from which it received the event
# (i.e. the id of our neuron).                                   

# Connecting nodes with default connections
nest.Connect(multimeter, neuron)
nest.Connect(neuron, spikedetector)
nest.Simulate(1000.0) # in ms

# Create an extra neuron
neuron2 = nest.Create("iaf_psc_alpha")
nest.SetStatus(neuron2,{"I_e":370.0})
# connect this newly created neuron to the multimeter
nest.Connect(multimeter, neuron2)
nest.Connect(neuron2, spikedetector)
# plot the results
dmm = nest.GetStatus(multimeter)[0]
Vms1 = dmm["events"]["V_m"][::2]
ts1 = dmm["events"]["times"][::2]
pl.plot(ts1,Vms1)
Vms2 = dmm["events"]["V_m"][1::2]
ts2 = dmm['events']['times'][1::2]
pl.plot(ts2,Vms2)
pl.show()
# ------------------------------------------------------- #
